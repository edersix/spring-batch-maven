package com.cruz.cesar.springbatchmaven.processor;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FirstItemProccesor implements ItemProcessor<Integer,Long> {
    @Override
    public Long process(Integer item) throws Exception {
        System.out.println("Entry parameter:"+item);
        List<Long> items= new ArrayList<>();
        test(items);
        System.out.println(items.get(items.size()-1));
        return Long.valueOf(item+20);
    }


    private void test(List<Long> items){
        items.add(Long.valueOf(20));
    }
}
