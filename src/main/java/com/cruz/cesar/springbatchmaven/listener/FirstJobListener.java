package com.cruz.cesar.springbatchmaven.listener;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

@Component
public class FirstJobListener implements JobExecutionListener {
    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println("Executed before job: "+jobExecution.getJobInstance().getJobName());
        System.out.println("Executed parameters: "+jobExecution.getJobParameters());
        System.out.println("Executed Context: "+jobExecution.getExecutionContext());

        jobExecution.getExecutionContext().put("param","sample value from listener");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("Executed after job :"+jobExecution.getJobInstance().getJobName());
        System.out.println("Executed Context: "+jobExecution.getExecutionContext());

    }
}
