package com.cruz.cesar.springbatchmaven.listener;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

@Component
public class FirstStepListener implements StepExecutionListener {
    @Override
    public void beforeStep(StepExecution stepExecution) {
        System.out.println("Executed before step: "+stepExecution.getStepName());
        System.out.println("Executed before step: "+stepExecution.getJobExecution().getExecutionContext());

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println("Executed before step: "+stepExecution.getStepName());
        return null;
    }
}
