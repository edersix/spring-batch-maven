package com.cruz.cesar.springbatchmaven.writer;

import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FirstItemWriter implements ItemWriter<Long> {
    @Override
    public void write(List<? extends Long> items) throws Exception {// size of da list depends on chunk size

        items.stream().forEach(item -> System.out.println(item));

    }
}
