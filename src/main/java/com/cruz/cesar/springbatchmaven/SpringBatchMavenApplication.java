package com.cruz.cesar.springbatchmaven;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableBatchProcessing
@ComponentScan({
		"com.cruz.cesar.springbatchmaven.config",
		"com.cruz.cesar.springbatchmaven.service",
		"com.cruz.cesar.springbatchmaven.listener",
		"com.cruz.cesar.springbatchmaven.reader",
		"com.cruz.cesar.springbatchmaven.writer",
		"com.cruz.cesar.springbatchmaven.processor",
		"com.cruz.cesar.springbatchmaven.controller"
})
public class SpringBatchMavenApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringBatchMavenApplication.class, args);
	}

}
