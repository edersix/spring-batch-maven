package com.cruz.cesar.springbatchmaven.config;

import com.cruz.cesar.springbatchmaven.listener.FirstJobListener;
import com.cruz.cesar.springbatchmaven.listener.FirstStepListener;
import com.cruz.cesar.springbatchmaven.processor.FirstItemProccesor;
import com.cruz.cesar.springbatchmaven.reader.FirstItemReader;
import com.cruz.cesar.springbatchmaven.service.SecondTaskLet;
import com.cruz.cesar.springbatchmaven.writer.FirstItemWriter;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SampleJob {

    public static final int CHUNK_SIZE = 3;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job firstJob(JobBuilderFactory jobBuilderFactory,
                        SecondTaskLet secondTaskLet,
                        FirstJobListener firstJobListener,
                        FirstStepListener firstStepListener
    ){
        //creating the job and defining its name
         return jobBuilderFactory.get("first Job")
                .start(firstStep(firstStepListener))
                 .next(secondStep(secondTaskLet))
                 .listener(firstJobListener)
                .build();
    }

    //
    private Step firstStep(FirstStepListener firstStepListener){
        //creating the step and defining its name
        return stepBuilderFactory.get("first Step")
                .tasklet(firstTask())
                .listener(firstStepListener)
                .build();
    }


    private Step secondStep(SecondTaskLet secondTaskLet){
        //creating the step and defining its name
        return stepBuilderFactory.get("second Step")
                .tasklet(secondTaskLet)
                .build();
    }

    //define what task we want to perform inside this step
    private Tasklet firstTask(){
        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {

                System.out.println("this is task let for step 1");
                return RepeatStatus.FINISHED;
            }
        };
    }

//    private Tasklet secondTask(){
//        return new Tasklet() {
//            @Override
//            public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
//
//                System.out.println("this is  task let for step 2");
//                return RepeatStatus.FINISHED;
//            }
//        };
//    }
    @Bean
    public Job secondJob(JobBuilderFactory jobBuilderFactory,
                       FirstItemReader firstItemReader,
                         FirstItemProccesor firstItemProccesor,
                         FirstItemWriter firstItemWriter
    ){
        return jobBuilderFactory.get("Second Job")
                .incrementer(new RunIdIncrementer())
                .start(firstChunkStep(firstItemReader,firstItemProccesor,firstItemWriter))
                .build();
    }

    private Step firstChunkStep( FirstItemReader firstItemReader,
                                 FirstItemProccesor firstItemProccesor,
                                 FirstItemWriter firstItemWriter){
        //creating the step and defining its name
        return stepBuilderFactory.get("first chunk Step")
                .<Integer,Long>chunk(CHUNK_SIZE)
                .reader(firstItemReader)
                .processor(firstItemProccesor)
                .writer(firstItemWriter)
                .build();
    }

}
